//
//  LoginView.swift
//  Mirra
//
//  Created by Ani Klekchyan on 5/6/20.
//  Copyright © 2020 Ani Klekchyan. All rights reserved.
//

import SwiftUI

struct LoginView: View {
    var body: some View {
        
        Text("Welcome")
            .foregroundColor(Color.purple)
            .multilineTextAlignment(.center)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
