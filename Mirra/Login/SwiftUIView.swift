//
//  SwiftUIView.swift
//  Mirra
//
//  Created by Ani Klekchyan on 5/6/20.
//  Copyright © 2020 Ani Klekchyan. All rights reserved.
//

import SwiftUI

struct SwiftUIView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
