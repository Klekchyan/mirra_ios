//
//  WelcomeView.swift
//  Mirra
//
//  Created by Ani Klekchyan on 5/6/20.
//  Copyright © 2020 Ani Klekchyan. All rights reserved.
//

import SwiftUI

struct WelcomeView: View {
    var body: some View {
        ZStack {
            Image("welcomeBackground")
            VStack {
                Text("Welcome!")
                    .font(.system(size: 24))
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                
                Text("You're moments away from confidence through private, relevant, and immediate feedback.")
                    .multilineTextAlignment(.center)
                    .font(.system(size: 18))
                    .foregroundColor(.white)
                    .lineLimit(5)
                    .frame(width: 250)
                
                    Button("JOIN NOW", action: {})
                        .foregroundColor(.white)
                        .font(.system(size: 30))
                        .frame(width: 264, height: 75)
                
            }.padding()
        }
    }
}

struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}
