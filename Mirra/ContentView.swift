//
//  ContentView.swift
//  Mirra
//
//  Created by Ani Klekchyan on 5/4/20.
//  Copyright © 2020 Ani Klekchyan. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State private var text: String = "Some text"
    @State private var isRed: Bool = false
    var body: some View {
        VStack{
            Text("Hello, World!")
            TextField("Placeholder", text: $text).foregroundColor(isRed ? .red : .green)
            Button("Click me!") {
                self.text = "Hi" + self.text
                self.isRed = !self.isRed
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
